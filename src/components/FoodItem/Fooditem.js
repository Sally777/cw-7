import React, {Component} from 'react';
import './Fooditem.css';
class Fooditem extends Component{
    render() {
        return (
            <button className="Fooditem">
                <h2>{this.props.name}</h2>
                <p>Price: {this.props.price}</p>
            </button>
        )
    }
}

export default Fooditem;


